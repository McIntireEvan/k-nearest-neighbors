﻿#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace K_Nearest_Neighbors
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        int k;
        MouseState previousMouseState;
        KeyboardState previousKeyboardState;
        Texture2D colorMap;

        List<Node> neighbors;
        Color nodeColor;
        Color[] data;

        SpriteFont f;

        public static Texture2D t;

        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager( this );
            Content.RootDirectory = "Content";
            k = 1;
        }

        protected override void Initialize()
        {
            previousMouseState = Mouse.GetState();
            previousKeyboardState = Keyboard.GetState();
            neighbors = new List<Node>();
            colorMap = new Texture2D( GraphicsDevice, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight );
            nodeColor = Color.Red;
            base.Initialize();
            data = new Color[colorMap.Width * colorMap.Height];
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch( GraphicsDevice );
            t = Content.Load<Texture2D>( "node.png" );
            f = Content.Load<SpriteFont>( "Font" );
            this.IsMouseVisible = true;
        }

        //TODO: Clean up mess here
        protected override void Update( GameTime gameTime )
        {
            if (GamePad.GetState( PlayerIndex.One ).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown( Keys.Escape ))
                Exit();

            MouseState currentState = Mouse.GetState();

            if (currentState.LeftButton == ButtonState.Pressed && previousMouseState.LeftButton == ButtonState.Released)
            {
                neighbors.Add( new Node( new Vector2( currentState.Position.X, currentState.Position.Y ), nodeColor ) );
                previousMouseState = currentState;
                genTexture();
            }
            if (currentState.RightButton == ButtonState.Pressed)
            {
                foreach (Node n in neighbors)
                {
                    if (Vector2.Distance( new Vector2( currentState.Position.X, currentState.Position.Y ), n.getPos() ) < 25)
                    {
                        neighbors.Remove( n );
                        genTexture();
                        break;
                    }
                }
            }
            if (currentState.LeftButton == ButtonState.Released)
            {
                previousMouseState = currentState;
            }

            KeyboardState keyState = Keyboard.GetState();
            if (keyState.IsKeyDown( Keys.NumPad1 ))
            {
                nodeColor = Color.Red;
            }
            if (keyState.IsKeyDown( Keys.NumPad2 ))
            {
                nodeColor = Color.Green;
            }
            if (keyState.IsKeyDown( Keys.NumPad3 ))
            {
                nodeColor = Color.Blue;
            }

            if (keyState.IsKeyDown( Keys.OemPlus ) && !previousKeyboardState.IsKeyDown( Keys.OemPlus ))
            {
                k++;
                previousKeyboardState = keyState;
                genTexture();
            }
            if (keyState.IsKeyDown( Keys.OemMinus ) && !previousKeyboardState.IsKeyDown( Keys.OemMinus ))
            {
                k--;
                previousKeyboardState = keyState;
                genTexture();
            }

            if (keyState.IsKeyUp( Keys.OemPlus ) && keyState.IsKeyUp( Keys.OemMinus ))
            {
                previousKeyboardState = keyState;
            }

            if (keyState.IsKeyDown( Keys.C ))
            {
                neighbors.Clear();
                genTexture();
            }

            base.Update( gameTime );
        }

        public void genTexture()
        {
            //TODO: Detect what actually needs to be updated, instead of looping through everything

            for (int i = 0; i < data.Length; i++)
            {
                int y = (int)((float)i / (float)colorMap.Width);

                int x = i % colorMap.Width;
                data[i] = ColorAt( new Vector2( x, y ) );
            }
            colorMap.SetData<Color>( data );
        }

        private Color ColorAt( Vector2 pos )
        {
            if (neighbors.ToArray().Length == 0)
            {
                return Color.White;
            }
            Dictionary<Node, double> distances = new Dictionary<Node, double>();
            foreach (Node n in neighbors)
            {
                distances.Add( n, Vector2.Distance( pos, n.getPos() ) );
            }
            var sorted = from entry in distances orderby entry.Value ascending select entry;
            distances = sorted.ToDictionary( entry => entry.Key, entry => entry.Value );

            float r = 0, g = 0, b = 0;

            for (int i = 0; i < Math.Min( k, neighbors.ToArray().Length ); i++)
            {
                Color c = distances.Keys.ElementAt( i ).getColor();
                r += c.R;
                g += c.G;
                b += c.B;
            }

            r /= k;
            g /= k;
            b /= k;

            return new Color( r, g, b );
        }

        protected override void Draw( GameTime gameTime )
        {
            GraphicsDevice.Clear( Color.White );

            spriteBatch.Begin();
            spriteBatch.Draw( colorMap, Vector2.Zero, Color.White );
            spriteBatch.DrawString( f, "k:" + k, Vector2.Zero, Color.Black );
            foreach (Node n in neighbors)
            {
                n.Draw( spriteBatch );
            }
            spriteBatch.End();
            base.Draw( gameTime );
        }
    }
}