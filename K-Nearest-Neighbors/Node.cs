﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace K_Nearest_Neighbors
{
    public class Node
    {
        private Vector2 position;
        private Color color;
        private Rectangle rect;

        public Node(Vector2 position, Color color)
        {
            this.position = new Vector2(position.X - (Game1.t.Width /2), position.Y - (Game1.t.Height /2));
            this.color = color;
            rect = new Rectangle((int)position.X, (int)position.Y, 10, 10);
        }

        public Vector2 getPos()
        {
            return this.position;
        }

        public Color getColor()
        {
            return this.color;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Game1.t, rect, color);
        }
    }
}